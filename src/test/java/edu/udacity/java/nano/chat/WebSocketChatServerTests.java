package edu.udacity.java.nano.chat;


import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

import javax.websocket.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebSocketChatServerTests {

    /** Test whn a user JOINS the chat room
     * onOpen, a user should be added to the chat group
     * A notifcation "{username} joined chat room" should be sent to every connected user
     */
    @Test
    public void onOpenShouldAddAuserToTheChatSessions() {
        //declaring request parameter map
        String username = "oyinkan";
        Map<String, List<String>> requestParameterMap = new HashMap<>();
        List<String> parameterList = new ArrayList<>();
        parameterList.add(username);
        requestParameterMap.put("username", parameterList);

        //mocking a websocket session and its getRequestParameterMap method
        Session session = mock(Session.class);
        when(session.getRequestParameterMap()).thenReturn(requestParameterMap);

        //create instance of a webSocketChatServer and call the OnOpen method on it
        WebSocketChatServer webSocketChatServer = new WebSocketChatServer();
        webSocketChatServer.onOpen(session);

        // A new user has been added to the chat
        assert WebSocketChatServer.onlineSessions.size() == 1;
        //The right user is the one added to the chat
        assert WebSocketChatServer.onlineSessions.containsKey(username);

    }

    /** Test when a user LEAVES the chat room
     * Test that onClose removes a user's chat session
     */
    @Test
    public void onCloseShouldRemoveSession() throws IOException {
        //declaring request parameter map
        String username = "oyinkan";
        Map<String, List<String>> requestParameterMap = new HashMap<>();
        List<String> parameterList = new ArrayList<>();
        parameterList.add(username);
        requestParameterMap.put("username", parameterList);


        //mocking a websocket session and its getRequestParameterMap method
        Session session = mock(Session.class);
        when(session.getRequestParameterMap()).thenReturn(requestParameterMap);

        //prepopulate chat sessions
        WebSocketChatServer.onlineSessions.put(username,session);
        //There is one active session
        assert WebSocketChatServer.onlineSessions.size() == 1;

        //create instance of a webSocketChatServer and call the onClose method on it
        WebSocketChatServer webSocketChatServer = new WebSocketChatServer();
        webSocketChatServer.onClose(session);

        // The appropriate session has been removed
        assert WebSocketChatServer.onlineSessions.size() == 0;
    }

    /**Test when a user CHATS e:e sends a message
     * @throws IOException
     */
    @Test
    public void onMessageShouldSendAMessageAllUsers() throws IOException {
        //declaring request parameter map
        String username = "oyinkan";
        String msg = "Hello";
        Map<String, List<String>> requestParameterMap = new HashMap<>();
        List<String> parameterList = new ArrayList<>();
        parameterList.add(username);
        requestParameterMap.put("username", parameterList);

        //prepare message
        JSONObject messageData = new JSONObject();
        messageData.put("username", username);
        messageData.put("msg", msg);
        messageData.put("type", "SPEAK");


        //mocking a websocket session and its getRequestParameterMap method
        Session session = mock(Session.class);
        RemoteEndpoint.Basic remoteEndpoint = mock(RemoteEndpoint.Basic.class);

        when(session.getRequestParameterMap()).thenReturn(requestParameterMap);
        when(session.isOpen()).thenReturn(true);
        when(session.getBasicRemote()).thenReturn(remoteEndpoint);

        //prepopulate chat sessions
        WebSocketChatServer.onlineSessions.put(username,session);

        //create instance of a webSocketChatServer and call the onMessage method on it
        WebSocketChatServer webSocketChatServer = new WebSocketChatServer();

        webSocketChatServer.onMessage(messageData.toJSONString());

        //message was sent to all active sessions
        //check that getBasicRemote() was called from " session.getBasicRemote().sendText(messageData.toJSONString())"
        verify(session).getBasicRemote();
    }



}