package edu.udacity.java.nano.chat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * WebSocket Server
 *
 * @see ServerEndpoint WebSocket Client
 * @see Session   WebSocket Session
 */

@Component
@ServerEndpoint("/chat")
public class WebSocketChatServer {

    /**
     * All chat sessions.
     */
    public static Map<String, Session> onlineSessions = new ConcurrentHashMap<>();

    /** Send message to every body in the chat room
     * @param msg
     * @param username
     */
    public  void sendMessageToAll(String msg, String username) {
        int onlineCount = onlineSessions.size();
        onlineSessions.forEach((s, session) -> {
            if (!session.isOpen()) {
                return;
            }
            JSONObject messageData = new JSONObject();
            messageData.put("username", username);
            messageData.put("msg", msg);
            messageData.put("onlineCount", onlineCount);
            messageData.put("type", "SPEAK");
            try {
                session.getBasicRemote().sendText(messageData.toJSONString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    /**
     * Open connection, 1) add session, 2) add user.
     */
    @OnOpen
    public void onOpen(Session session) {
        String username = session.getRequestParameterMap().get("username").get(0);
        onlineSessions.put(username, session);
        String welcomeMessage = username + " joined chat room";
        sendMessageToAll(welcomeMessage, "System");
    }

    /**
     * Send message, 1) get username and session, 2) send message to all.
     */
    @OnMessage
    public void onMessage(String jsonStr) {
        Map data = (Map) JSON.parse(jsonStr);
        sendMessageToAll((String) data.get("msg"), (String) data.get("username"));
    }

    /**
     * Close connection, 1) remove session .
     */
    @OnClose
    public void onClose(Session session) throws IOException {
        String username = session.getRequestParameterMap().get("username").get(0);
        String goodbyeMessage = username + " left chat room";
        onlineSessions.remove(username);
        sendMessageToAll(goodbyeMessage, "System");
        session.close();
    }

    /**
     * Print exception.
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

}
